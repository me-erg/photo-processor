#thumbnail generator
import io
import os
import pika
import psycopg2
import psycopg2.extras
import urllib.request
from PIL import Image

THUMBNAIL_PATH = "/waldo-app-thumbs"
QUEUE_NAME = "photo-processor"
db = None

def get_db():
    global db
    if db is None:
        try:
            db=psycopg2.connect(os.environ.get("PG_CONNECTION_URI"))
        except:
            print("Unable to connect to the database!")
    return db

def fetchone(query: str, params: tuple):
    cur = get_db().cursor(cursor_factory = psycopg2.extras.DictCursor)
    cur.execute(query, params)
    return cur.fetchone()

def query_execute(query: str, params: tuple):
    db = get_db()
    cur = db.cursor()
    cur.execute(query, params)
    db.commit()

def start_consumer():
    parameters = pika.URLParameters(os.environ.get("AMQP_URI"))
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=QUEUE_NAME, durable=True, exclusive=False, auto_delete=False)
    channel.basic_consume(queue=QUEUE_NAME, on_message_callback=process_photos)
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
        connection.close()

#start processing photos one by one
def process_photos(channel, method_frame, header_frame, body):
    id = str(body, 'utf-8')
    query = "SELECT * FROM photos WHERE uuid=%s AND status='pending'"
    row = fetchone(query, [id])
    print(row)
    if row is None:
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
        return
    # set status to processing
    query_execute("UPDATE photos SET status='processing' WHERE uuid=%s", [id])
    thumbnail(row)
    channel.basic_ack(delivery_tag=method_frame.delivery_tag)

#generate thumbnail
def thumbnail(row: dict):
    size = (320, 320)
    thumbnail_name = "%s.jpg" % row["uuid"]
    outfile = "%s/%s" % (THUMBNAIL_PATH, thumbnail_name)
    try:
        response = urllib.request.urlopen(row["url"])
        file_data = io.BytesIO(response.read())
        im = Image.open(file_data)
        im.thumbnail(size)
        im.save(outfile, "JPEG")
    except:
        query_execute("UPDATE photos SET status='failed' WHERE uuid=%s", [row["uuid"]])
        return
    width, height = im.size
    query_execute("UPDATE photos SET status='completed' WHERE uuid=%s", [row["uuid"]])
    query_execute("INSERT INTO photo_thumbnails (photo_uuid, width, height, url, created_at) VALUES(%s, %s, %s, %s, NOW())", [row["uuid"], width, height, thumbnail_name])

#initialize db connection
get_db()
start_consumer()
