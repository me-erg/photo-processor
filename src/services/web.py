from flask import Flask, jsonify, request, send_file
import pika
import db
import os
app = Flask(__name__)

app.config.from_object('config.Config')
app.app_context().push()

@app.route("/")
def index():
    return jsonify(success=True)

#Pending photos
@app.route("/photos/pending", methods = ["GET"])
def photos_pending():
    rows = db.fetchall("SELECT * FROM photos WHERE status='pending'")
    return jsonify(rows)

@app.route("/photos/process", methods = ["POST"])
def photos_process():
    uuids = request.get_json().get("uuid")

    parameters = pika.URLParameters(app.config["AMQP_URI"])
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="photo-processor", durable=True, exclusive=False, auto_delete=False)

    for uuid in uuids:
        channel.basic_publish(exchange="",
            routing_key="photo-processor",
            body=uuid,
            properties=pika.BasicProperties(content_type='text/plain',
                                           delivery_mode=1))
    connection.close()
    return jsonify(success=True)

@app.route("/photos/thumbnail/<string:id>", methods = ["GET"])
def photos_thumbnail(id):
    filename = "%s/%s.jpg" % (app.config["THUMBNAIL_PATH"], id)
    return send_file(filename, mimetype='image/jpeg')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000)
