import psycopg2
import psycopg2.extras
from flask import current_app as app, g
from flask.cli import with_appcontext

def get_db():
    if 'db' not in g:
        try:
            g.db=psycopg2.connect(app.config['PG_CONNECTION_URI'])
        except:
            print("Unable to connect to the database!")
    return g.db

def fetchall(query: str, params=()):
    cur = get_db().cursor(cursor_factory = psycopg2.extras.DictCursor)
    cur.execute(query, params)
    return cur.fetchall()
